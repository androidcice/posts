Posts

La práctica consistirá en una aplicación que resumirá lo que hemos visto hasta ahora.

1 – Splash:

Tras 3 segundos, si es la primera vez que abro la aplicación voy a la pantalla 2 “tutorial”, si ya he abierto la aplicación alguna vez voy a el “main” de la aplicación.

2 – Tutorial con un “viewpager” con constará de 3 páginas:

Página 1 “POSTS”
Página 2 “ALBUMS”
Página 3 “USERS”

Cada página consta de una imagen centrada horizontalmente en la parte alta de la pantalla, el título correspondiente debajo y los botones “siguiente”, “siguiente”, “entendido” respectivamente.

Además en todas ellas, en la parte superior derecha habrá un texto “saltar”.

Los botones “siguiente” nos llevan a la siguiente página, mientras que “entendido” y “saltar” a la pantalla principal de la aplicación.

3- Main:

Constará de un menú lateral con los campos, “POSTS”, “ALBUMS”, “USERS” Y “TUTORIAL”
Al pulsar sobre cualquier de ellos excepto del de tutorial, se mostrará un listado con la información recibida del api que dejo a continuación.
Al pulsar tutorial se volverá mostrar el tutorial del comienzo.

3.1- POSTS:

        - Deberá aparecer el listado de todos los posts recibidos del api, y también una caja de texto con un botón para filtrar por userid y mostrar solo los posts con ese userid. Al pulsar sobre uno de estos post se deberá ir a una pantalla de detalle que muestre la información del post así como los comentarios asociados al mismo.

Además se debo poder crear un nuevo post así como eliminar uno ya existente ( esta información no se persiste en el api, pero debemos obtener un código de respuesta OK) Y mostrar un Dialog con la opción de borrar / crear otro post, o solo la opción de cerra el Dialog

3.2- ALBUMS:

        - Deberá aparece un listado con todos los albums recibidos del api y al pulsar en uno de ellos un listado con todas las fotos asociadas a ese álbum

3.3- USERS:

        - Deberá aparece un listado con todos los usuarios, mostrando solo dos campos que se consideren relevantes. Al pulsar sobre un usuario se deberá mostrar toda la información del mismo.

El diseño es 100% libre, pero se debe evitar la pérdida de información, solapamiento de datos etc…

Página del api:

https://jsonplaceholder.typicode.com/