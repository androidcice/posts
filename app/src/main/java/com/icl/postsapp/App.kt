package com.icl.postsapp

import android.app.Application
import android.util.Log

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.d("POSTSAPP", "La aplicación se ha iniciado")
    }
}