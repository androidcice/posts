package com.icl.postsapp.usescases.main

import android.os.Bundle
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity

class MainActivity : GlobalActivity() {


    // region LifeCircle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadFragment()
    }

    // endregion LifeCircle

    // region UI



    //endregion UI

    // region data


    // endregion data


}
