package com.icl.postsapp.usescases.extensions

import android.widget.ImageView
import com.icl.postsapp.usescases.globals.GlideApp


fun ImageView.setImageURL(url: String?,imageView: ImageView) {
        GlideApp.with(context)
            .load(url)
            .override(600, 600)
            .into(imageView)

}

