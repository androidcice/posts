package com.icl.postsapp.usescases.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.icl.postsapp.R
import com.icl.postsapp.constants.Constants
import com.icl.postsapp.data.PreferencesUtils
import com.icl.postsapp.usescases.main.MainActivity
import com.icl.postsapp.usescases.tutorial.TutorialActivity


class SplashActivity : AppCompatActivity() {

    //region LYFE CYCLE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Thread.sleep(3000)

        var intent: Intent
        if(!checkIfIsFirstTime()) {
            intent = Intent(this@SplashActivity, TutorialActivity::class.java)
            startRecord()
        }
        else
            intent = Intent(this@SplashActivity, MainActivity::class.java)

        startActivity(intent)

    }

    //endregion LYFE CYCLE

    private fun checkIfIsFirstTime(): Boolean {

        val result = PreferencesUtils(this.applicationContext).getPreference(Constants.FIRST_TIME)
        result?.forEach {
            if(it.equals("0")){
                return true
            }
            return false
        }
        return false
    }

    private fun startRecord(){
        PreferencesUtils(this.applicationContext).savePreferences("1", Constants.FIRST_TIME)
    }

}
