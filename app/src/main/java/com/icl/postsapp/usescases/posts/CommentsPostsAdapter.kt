package com.icl.postsapp.usescases.posts

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icl.postsapp.R
import kotlinx.android.synthetic.main.item_comments_post.view.*
import java.util.*

class CommentsPostsAdapter(private val items: ArrayList<Comments>, private  val context: Context) : RecyclerView.Adapter<ViewCommentsHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewCommentsHolder {
        return ViewCommentsHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_comments_post, p0, false))
    }


    override fun onBindViewHolder(p0: ViewCommentsHolder, p1: Int) {
        p0.bindItems(items.get(p1), context)
    }

}


class ViewCommentsHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: Comments, context: Context) {
    /*    itemView.postsParentId.text = parent.id.toString()
        itemView.postsParentTitle.text= parent.title
        itemView.postsParentBody.text=parent.body
        *//*itemView.postsBody.maxLines=5*//*
        itemView.postsParentIdUser.text=parent.id.toString()*/
        itemView.commentName.text= item.name
        itemView.commentBody.text= item.body
        itemView.commentId.text=item.id.toString()
        itemView.commentIdUser.text=item.email

/*        itemView.setOnClickListener {
            var intent = Intent(context, PostsDetailActivity::class.java)
            intent.putExtra("post", item)
            context.startActivity(intent)
        }*/

    }

}