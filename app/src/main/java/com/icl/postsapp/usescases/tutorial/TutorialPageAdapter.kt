package com.icl.postsapp.usescases.tutorial

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class TutorialPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {



    override fun getCount() = 3

    override fun getItem(position: Int): Fragment {
        return when (position % 3) {
            0 -> FragmentPage.newInstance(0)
            1 -> FragmentPage.newInstance(1)
            else ->  FragmentPage.newInstance(2)
        }
    }



}