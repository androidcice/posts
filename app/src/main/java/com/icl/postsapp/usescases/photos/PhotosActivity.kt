package com.icl.postsapp.usescases.photos

import android.os.Bundle
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity

class PhotosActivity : GlobalActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)
    }
}
