package com.icl.postsapp.usescases.posts

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comments(
    val postId: Long,
    val id: Long,
    val name: String,
    val email: String,
    val body: String
) : Parcelable
