package com.icl.postsapp.usescases.photos

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icl.postsapp.R
import com.icl.postsapp.usescases.extensions.setImageURL
import kotlinx.android.synthetic.main.item_photos.view.*
import java.util.*

class PhotosAdapter(private val items: ArrayList<Photo>, private  val context: Context) : RecyclerView.Adapter<ViewPhotosHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewPhotosHolder {
        return ViewPhotosHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_photos, p0, false))
    }


    override fun onBindViewHolder(p0: ViewPhotosHolder, p1: Int) {
        p0.bindItems(items.get(p1), context)
    }

}


class ViewPhotosHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: Photo, context: Context) {
        itemView.photoId.text = item.id.toString()
        var imageView = itemView.photoItem
        itemView.photoItem.setImageURL("https://via.placeholder.com/600/92c952", imageView)

/*
        itemView.commentBody.text= item.body
        itemView.
            .text= item.name
*/


    }

}