package com.icl.postsapp.usescases.tutorial

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity
import com.icl.postsapp.usescases.main.MainActivity
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.activity_tutorial.view.*



class TutorialActivity : GlobalActivity(), ViewPager.OnPageChangeListener {


   /*private var pageListener: TutorialPageListener= TutorialPageListener()*/
   var currentPage: Int = 0

    //region LYFE CYCLE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.icl.postsapp.R.layout.activity_tutorial)
        definePager()
        nextButton()
        changeNextButtonOnEnd(0)

    }

    //endregion LYFE CYCLE

    //region UI
    private fun definePager(){

        viewPager.adapter = TutorialPageAdapter(supportFragmentManager)
        viewPager.addOnPageChangeListener(this)

    }

    private fun changeNextButtonOnEnd(position: Int) {
        if (position == viewPager.adapter?.count?.minus(1) ?: viewPager.currentItem) {
            next.text = getString(R.string.understand)
        } else {
            next.text = getString(R.string.next)
        }
    }

    fun jumpButton(view : View){
        var intent = Intent(this@TutorialActivity, MainActivity::class.java)
        startActivity(intent)
    }

    private fun nextButton(){

        tutorialView.next.setOnClickListener {

            var count = currentPage
            if (count==1)
                next.text = getString(R.string.understand)
            else
                next.text=getString(com.icl.postsapp.R.string.next)

            if (count==2) {
                var intent = Intent(this@TutorialActivity, MainActivity::class.java)
                startActivity(intent)
            }
            viewPager.setCurrentItem(currentPage+1, true)
        }
    }
    //endregion UI

    //region PageListener
    override fun onPageSelected(position: Int) {
        Log.i("POSTSAPP - OnPageSelected", "page selected $position")
        currentPage = position
        changeNextButtonOnEnd(currentPage)

    }

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
    }
    //endregion PageListener
}
