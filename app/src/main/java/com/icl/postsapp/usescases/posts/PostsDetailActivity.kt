package com.icl.postsapp.usescases.posts

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity
import kotlinx.android.synthetic.main.activity_posts_detail.*

class PostsDetailActivity : GlobalActivity() {

    // region LifeCircle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts_detail)
        var post = intent.getParcelableExtra<Posts>("post")
        postsParentId.text = post.id.toString()
        postsParentTitle.text = post.title
        postsParentBody.text = post.body
        recyclerPostDetail.layoutManager = LinearLayoutManager(this)
        /*itemView.postsBody.maxLines=5*/
        /*postsParentIdUser.text=post.id.toString()*/
        var list: ArrayList<Comments> = arrayListOf()
        var comment = Comments(12, 24, "Dummy", "Dummy email", "Dummy body")
        list.add(comment)


        var commentsPostsAdapter: CommentsPostsAdapter = CommentsPostsAdapter(list, this@PostsDetailActivity)
        recyclerPostDetail.adapter = commentsPostsAdapter

    }

    // endregion LifeCircle

    // region UI

    // endregion UI
}
