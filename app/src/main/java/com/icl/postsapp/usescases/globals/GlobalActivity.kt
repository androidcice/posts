package com.icl.postsapp.usescases.globals

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.icl.postsapp.R
import com.icl.postsapp.usescases.albums.Albums
import com.icl.postsapp.usescases.albums.AlbumsAdapter
import com.icl.postsapp.usescases.posts.Posts
import com.icl.postsapp.usescases.posts.PostsAdapter
import com.icl.postsapp.usescases.tutorial.TutorialActivity
import com.icl.postsapp.usescases.users.*
import kotlinx.android.synthetic.main.activity_main.*

abstract class GlobalActivity : AppCompatActivity() {

    lateinit var postsList: ArrayList<Posts>
    lateinit var albumsList: ArrayList<Albums>
    lateinit var usersList: ArrayList<Users>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("Init POSTSAPP", localClassName)
    }


    // region nav

    fun loadFragment() {
        postsList = getPosts()
        albumsList = getAlbums()
        usersList = getUsers()

        container.layoutManager = LinearLayoutManager(this)

        navView.setNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.op1 -> {
                    Log.d("OP1", "POSTS")
                    var postsAdapter = PostsAdapter(postsList, this@GlobalActivity)
                    container.adapter = postsAdapter
                }
                R.id.op2 -> {
                    Log.d("OP2", "ALBUMS")
                    var albumsAdapter = AlbumsAdapter(albumsList, this@GlobalActivity)
                    container.adapter= albumsAdapter
                }
                R.id.op3 -> {
                    Log.d("OP3", "USERS")
                    var usersAdapter = UsersAdapter(usersList, this@GlobalActivity)
                    container.adapter = usersAdapter
                }
                R.id.op4 ->{
                    Log.d("OP4", "TUTORIAL")
                    var intent = Intent(this@GlobalActivity, TutorialActivity::class.java)
                    startActivity(intent)
                }
            }
            false

        }

    }

    // endregion nav

    // region data

    fun getPosts(): ArrayList<Posts> {
        postsList = arrayListOf()
        var post = Posts(1, 32, "Prueba", "Prueba")
        postsList.add(post)
        return postsList
    }

    fun getAlbums(): ArrayList<Albums> {
        albumsList = arrayListOf()
        var album = Albums(1, 32, "Prueba")
        albumsList.add(album)
        return albumsList
    }

    fun getUsers(): ArrayList<Users> {
        usersList = arrayListOf()
        var user = Users(
            1, "Leanne Graham", "Bret", "Sincere@april.biz",
            Address(
                "Kulas Light"
                , "Apt. 556"
                , "Gwenborough"
                , "92998-3874",
                Geo("-37.3159", "81.1496")
            )

            , "1-770-736-8031 x56442"
            , "hildegard.org",
            Company(
                "Romaguera-Crona"
                , "Multi-layered client-server neural-net"
                , "harness real-time e-markets"
            )
        )
        usersList.add(user)
        usersList.add(user)
        usersList.add(user)
        usersList.add(user)
        usersList.add(user)

        return usersList
    }

    // endregion data
}

