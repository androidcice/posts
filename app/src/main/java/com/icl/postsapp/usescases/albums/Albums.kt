package com.icl.postsapp.usescases.albums

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Albums(
    val userId: Long,
    val id: Long,
    val title: String
) : Parcelable
