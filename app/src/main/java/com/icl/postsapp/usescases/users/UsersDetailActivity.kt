package com.icl.postsapp.usescases.users

import android.os.Bundle
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity
import kotlinx.android.synthetic.main.activity_users_detail.*

class UsersDetailActivity : GlobalActivity() {


    // region LifeCircle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_detail)
        var user = intent.getParcelableExtra<Users>("user")
        inflateUser(user)
    }

    // endregion LifeCircle

    // region UI

    private fun inflateUser(user: Users) {

        userPhone.text = user.phone
        userEmail.text = user.email
        userHome.text = user.address.street
        userName.text = user.name
        userNameApp.text = user.username
        userSite.text = user.website
    }



    // endregion UI
}


