package com.icl.postsapp.usescases.users

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icl.postsapp.R
import kotlinx.android.synthetic.main.item_users.view.*
import java.util.*

class UsersAdapter(private val items: ArrayList<Users>, private  val context: Context) : RecyclerView.Adapter<ViewUsersHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewUsersHolder {
        return ViewUsersHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_users, p0, false))
    }


    override fun onBindViewHolder(p0: ViewUsersHolder, p1: Int) {
        p0.bindItems(items.get(p1), context)
    }

}


class ViewUsersHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: Users, context: Context) {
        itemView.nameText.text = item.name
        itemView.emailText.text= item.email
        itemView.setOnClickListener {
            var intent = Intent(context, UsersDetailActivity::class.java)
            intent.putExtra("user", item)
            context.startActivity(intent)
        }
    }

}