package com.icl.postsapp.usescases.tutorial

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.icl.postsapp.R
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.page_tutorial.view.*


class FragmentPage : Fragment() {

    var nextButton: Button? =null

    companion object {
        const val POSITION = "position"

        fun newInstance(text: Int): FragmentPage {
            val arguments = Bundle()
            arguments.putInt(POSITION, text)
            val fragment = FragmentPage()
            fragment.arguments = arguments
            return fragment
        }
    }

    //region lifeCircle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("Tutorial POSTSAPP", "onCreateView")
        var view = inflater.inflate(com.icl.postsapp.R.layout.page_tutorial, container, false)

        nextButton= (view.context as Activity).next
        var pos=arguments?.getInt(POSITION)?:0

        view.titleText.text=getTitle(pos)
        /*checkPositionForNext(pos)*/
        changeImage(pos, view)
        return view
    }

    override fun onResume() {
        super.onResume()
        this.isVisible
        Log.d("Tutorial POSTSAPP", "onResume")
    }


    private fun getTitle(position:Int): String = when (position) {
        0 -> getString(R.string.postsTitle)
        1 -> getString(R.string.albumsTitle)
        else ->  getString(R.string.usersTitle)
    }

    private fun changeImage(position:Int, view: View){
        when(position){
            0-> view!!.image.setImageResource(R.drawable.posts)
            1 -> view!!.image.setImageResource(R.drawable.albums)
            else -> view!!.image.setImageResource(R.drawable.users)
        }

    }


  /*  private fun checkPositionForNext(position:Int){
        *//* TODO No hace caso el posicionamiento y pone lo que quiere, además en la ultima posicion no pasa por aqui....
        Consultar a Javier*//*
        when(position){
            0-> nextButton!!.text=getString(R.string.next)
            1 -> nextButton!!.text=getString(R.string.next)
            else ->  nextButton!!.text=getString(R.string.understand)
        }
     *//*   if(currentPage==2)
            nextButton!!.text="entendido"
        else
            nextButton!!.text="siguiente"*//*
    }*/
}