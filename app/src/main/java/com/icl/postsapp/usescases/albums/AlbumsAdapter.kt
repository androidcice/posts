package com.icl.postsapp.usescases.albums

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icl.postsapp.R
import kotlinx.android.synthetic.main.item_albums.view.*
import java.util.*

class AlbumsAdapter(private val items: ArrayList<Albums>, private  val context: Context) : RecyclerView.Adapter<ViewAlbumHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewAlbumHolder {
        return ViewAlbumHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_albums, p0, false))
    }


    override fun onBindViewHolder(p0: ViewAlbumHolder, p1: Int) {
        p0.bindItems(items.get(p1), context)
    }

}


class ViewAlbumHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: Albums, context: Context) {
        itemView.albumId.text = item.id.toString()
        itemView.albumTitle.text= item.title
        itemView.setOnClickListener {
            var intent = Intent(context, AlbumsDetailActivity::class.java)
            intent.putExtra("album", item)
            context.startActivity(intent)
        }

    }

}