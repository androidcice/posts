package com.icl.postsapp.usescases.albums

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.icl.postsapp.R
import com.icl.postsapp.usescases.globals.GlobalActivity
import com.icl.postsapp.usescases.photos.Photo
import com.icl.postsapp.usescases.photos.PhotosAdapter
import kotlinx.android.synthetic.main.activity_albums_detail.*

class AlbumsDetailActivity : GlobalActivity() {

    // region LifeCircle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_albums_detail)
        var album = intent.getParcelableExtra<Albums>("album")
        recyclerAlbumsDetail.layoutManager = LinearLayoutManager(this)
        var list: ArrayList<Photo> = arrayListOf()
        var photo =
            Photo(1, 24, "Dummy", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/600/92c952")
        list.add(photo)
        list.add(photo)
        list.add(photo)
        list.add(photo)
        list.add(photo)
        list.add(photo)


        var photosAdapter = PhotosAdapter(list, this@AlbumsDetailActivity)
        recyclerAlbumsDetail.adapter = photosAdapter

    }

    // endregion LifeCircle

    // region UI

    // endregion UI
}
