package com.icl.postsapp.usescases.posts

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Posts(
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String
) : Parcelable