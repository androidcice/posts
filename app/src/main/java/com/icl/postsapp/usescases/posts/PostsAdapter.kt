package com.icl.postsapp.usescases.posts

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.icl.postsapp.R
import kotlinx.android.synthetic.main.item_posts.view.*
import java.util.*

class PostsAdapter(private val items: ArrayList<Posts>, private  val context: Context) : RecyclerView.Adapter<ViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_posts, p0, false))
    }


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItems(items.get(p1), context)
    }

}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(item: Posts, context: Context) {
        itemView.postsId.text = item.id.toString()
        itemView.postsTitle.text= item.title
        itemView.postsBody.text=item.body
        /*itemView.postsBody.maxLines=5*/
        itemView.postsIdUser.text=item.userId.toString()
        itemView.setOnClickListener {
            var intent = Intent(context, PostsDetailActivity::class.java)
            intent.putExtra("post", item)
            context.startActivity(intent)
        }

    }

}