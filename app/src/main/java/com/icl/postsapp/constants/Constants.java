package com.icl.postsapp.constants;

public class Constants {


    private Constants(){
        // default constructor
    }

    public static final String APP_NAME="POSTS_APP";
    public static final String STRING_EMPTY="";
    public static final String FIRST_TIME="FIRST_TIME";


    public static String logBuilder(String method, String extra){
        return String.format("Method: %1$s , desc: %2$s", method, extra);
    }

}
